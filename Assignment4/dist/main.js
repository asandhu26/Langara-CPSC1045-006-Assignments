/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
//Get input elements
//Get their values and store them in variables

var rotation = 0;

function update(){
    rotation = +document.querySelector("#range").value;
    var range = document.querySelector("#collection")
let rotateString = "rotate(" + rotation + " 190 100)"
    console.log(rotateString);
    collection.setAttribute("transform", rotateString);
    let xbox = document.querySelector("#x")
    let ybox = document.querySelector("#y")
    console.log(xbox);
    console.log(ybox);
 let translateString = "translate("+ xbox.value +"1 1"+ ybox.value+")";
    console.log(translateString)
 let transformString = translateString + " "+ rotateString;
    console.log(transformString);
    collection.setAttribute("transform", transformString)
}

window.update = update

function change(){
console.log("working")
let extendin= document.querySelector("#range2");
let extendout= document.querySelector("#range3");
let xloc = document.querySelector("#xloc");
let yloc = document.querySelector("#yloc");
let outextend = Number(extendout.value);
let inextend = Number(extendin.value);

let x1 = outextend * Math.cos(0)
let x2 = inextend * Math.cos(0.76)
let x3 = outextend * Math.cos(1.57)
let x4 = inextend * Math.cos(2.36)
let x5 = outextend * Math.cos(3.14)
let x6 = inextend * Math.cos(3.92)
let x7 = outextend * Math.cos(4.71)
let x8 = inextend * Math.cos(5.50)

let y1 = outextend * Math.sin(0)
let y2 = inextend * Math.sin(0.76)
let y3 = outextend * Math.sin(1.57)
let y4 = inextend * Math.sin(2.36)
let y5 = outextend * Math.sin(3.14)
let y6 = inextend * Math.sin(3.92)
let y7 = outextend * Math.sin(4.71)
let y8 = inextend * Math.sin(5.50)

xloc = Number(xloc.value)
yloc = Number(yloc.value)
let translatestar = "translate("+xloc+" "+yloc+")"
let pointsign = (x1+ " " +y1+ " " +x2+ " " +y2+ " " +x3+ " " +y3+ " " +x4+ " " +y4+ " " +x5+ " " +y5+ " " +x6+ " " +y6+ " " +x7+ " " +y7+ " " +x8+ " " +y8)
let polygon = document.querySelector("#polygon")
polygon.setAttribute("points",pointsign)
console.log("points")
polygon.setAttribute("transform",translatestar)
//get the object of g
//take the object of g and convert to translatestar
}
window.change = change

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsIi8vVGhpcyBpcyB0aGUgZW50cnkgcG9pbnQgSmF2YVNjcmlwdCBmaWxlLlxyXG4vL1dlYnBhY2sgd2lsbCBsb29rIGF0IHRoaXMgZmlsZSBmaXJzdCwgYW5kIHRoZW4gY2hlY2tcclxuLy93aGF0IGZpbGVzIGFyZSBsaW5rZWQgdG8gaXQuXHJcbmNvbnNvbGUubG9nKFwiSGVsbG8gd29ybGRcIik7XHJcbi8vR2V0IGlucHV0IGVsZW1lbnRzXHJcbi8vR2V0IHRoZWlyIHZhbHVlcyBhbmQgc3RvcmUgdGhlbSBpbiB2YXJpYWJsZXNcclxuXHJcbnZhciByb3RhdGlvbiA9IDA7XHJcblxyXG5mdW5jdGlvbiB1cGRhdGUoKXtcclxuICAgIHJvdGF0aW9uID0gK2RvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcmFuZ2VcIikudmFsdWU7XHJcbiAgICB2YXIgcmFuZ2UgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2NvbGxlY3Rpb25cIilcclxubGV0IHJvdGF0ZVN0cmluZyA9IFwicm90YXRlKFwiICsgcm90YXRpb24gKyBcIiAxOTAgMTAwKVwiXHJcbiAgICBjb25zb2xlLmxvZyhyb3RhdGVTdHJpbmcpO1xyXG4gICAgY29sbGVjdGlvbi5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIiwgcm90YXRlU3RyaW5nKTtcclxuICAgIGxldCB4Ym94ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN4XCIpXHJcbiAgICBsZXQgeWJveCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjeVwiKVxyXG4gICAgY29uc29sZS5sb2coeGJveCk7XHJcbiAgICBjb25zb2xlLmxvZyh5Ym94KTtcclxuIGxldCB0cmFuc2xhdGVTdHJpbmcgPSBcInRyYW5zbGF0ZShcIisgeGJveC52YWx1ZSArXCIxIDFcIisgeWJveC52YWx1ZStcIilcIjtcclxuICAgIGNvbnNvbGUubG9nKHRyYW5zbGF0ZVN0cmluZylcclxuIGxldCB0cmFuc2Zvcm1TdHJpbmcgPSB0cmFuc2xhdGVTdHJpbmcgKyBcIiBcIisgcm90YXRlU3RyaW5nO1xyXG4gICAgY29uc29sZS5sb2codHJhbnNmb3JtU3RyaW5nKTtcclxuICAgIGNvbGxlY3Rpb24uc2V0QXR0cmlidXRlKFwidHJhbnNmb3JtXCIsIHRyYW5zZm9ybVN0cmluZylcclxufVxyXG5cclxud2luZG93LnVwZGF0ZSA9IHVwZGF0ZVxyXG5cclxuZnVuY3Rpb24gY2hhbmdlKCl7XHJcbmNvbnNvbGUubG9nKFwid29ya2luZ1wiKVxyXG5sZXQgZXh0ZW5kaW49IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcmFuZ2UyXCIpO1xyXG5sZXQgZXh0ZW5kb3V0PSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JhbmdlM1wiKTtcclxubGV0IHhsb2MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3hsb2NcIik7XHJcbmxldCB5bG9jID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN5bG9jXCIpO1xyXG5sZXQgb3V0ZXh0ZW5kID0gTnVtYmVyKGV4dGVuZG91dC52YWx1ZSk7XHJcbmxldCBpbmV4dGVuZCA9IE51bWJlcihleHRlbmRpbi52YWx1ZSk7XHJcblxyXG5sZXQgeDEgPSBvdXRleHRlbmQgKiBNYXRoLmNvcygwKVxyXG5sZXQgeDIgPSBpbmV4dGVuZCAqIE1hdGguY29zKDAuNzYpXHJcbmxldCB4MyA9IG91dGV4dGVuZCAqIE1hdGguY29zKDEuNTcpXHJcbmxldCB4NCA9IGluZXh0ZW5kICogTWF0aC5jb3MoMi4zNilcclxubGV0IHg1ID0gb3V0ZXh0ZW5kICogTWF0aC5jb3MoMy4xNClcclxubGV0IHg2ID0gaW5leHRlbmQgKiBNYXRoLmNvcygzLjkyKVxyXG5sZXQgeDcgPSBvdXRleHRlbmQgKiBNYXRoLmNvcyg0LjcxKVxyXG5sZXQgeDggPSBpbmV4dGVuZCAqIE1hdGguY29zKDUuNTApXHJcblxyXG5sZXQgeTEgPSBvdXRleHRlbmQgKiBNYXRoLnNpbigwKVxyXG5sZXQgeTIgPSBpbmV4dGVuZCAqIE1hdGguc2luKDAuNzYpXHJcbmxldCB5MyA9IG91dGV4dGVuZCAqIE1hdGguc2luKDEuNTcpXHJcbmxldCB5NCA9IGluZXh0ZW5kICogTWF0aC5zaW4oMi4zNilcclxubGV0IHk1ID0gb3V0ZXh0ZW5kICogTWF0aC5zaW4oMy4xNClcclxubGV0IHk2ID0gaW5leHRlbmQgKiBNYXRoLnNpbigzLjkyKVxyXG5sZXQgeTcgPSBvdXRleHRlbmQgKiBNYXRoLnNpbig0LjcxKVxyXG5sZXQgeTggPSBpbmV4dGVuZCAqIE1hdGguc2luKDUuNTApXHJcblxyXG54bG9jID0gTnVtYmVyKHhsb2MudmFsdWUpXHJcbnlsb2MgPSBOdW1iZXIoeWxvYy52YWx1ZSlcclxubGV0IHRyYW5zbGF0ZXN0YXIgPSBcInRyYW5zbGF0ZShcIit4bG9jK1wiIFwiK3lsb2MrXCIpXCJcclxubGV0IHBvaW50c2lnbiA9ICh4MSsgXCIgXCIgK3kxKyBcIiBcIiAreDIrIFwiIFwiICt5MisgXCIgXCIgK3gzKyBcIiBcIiAreTMrIFwiIFwiICt4NCsgXCIgXCIgK3k0KyBcIiBcIiAreDUrIFwiIFwiICt5NSsgXCIgXCIgK3g2KyBcIiBcIiAreTYrIFwiIFwiICt4NysgXCIgXCIgK3k3KyBcIiBcIiAreDgrIFwiIFwiICt5OClcclxubGV0IHBvbHlnb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3BvbHlnb25cIilcclxucG9seWdvbi5zZXRBdHRyaWJ1dGUoXCJwb2ludHNcIixwb2ludHNpZ24pXHJcbmNvbnNvbGUubG9nKFwicG9pbnRzXCIpXHJcbnBvbHlnb24uc2V0QXR0cmlidXRlKFwidHJhbnNmb3JtXCIsdHJhbnNsYXRlc3RhcilcclxuLy9nZXQgdGhlIG9iamVjdCBvZiBnXHJcbi8vdGFrZSB0aGUgb2JqZWN0IG9mIGcgYW5kIGNvbnZlcnQgdG8gdHJhbnNsYXRlc3RhclxyXG59XHJcbndpbmRvdy5jaGFuZ2UgPSBjaGFuZ2UiXSwic291cmNlUm9vdCI6IiJ9