
let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers
    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;
    for (let e of containers) {
        f(e);
    }

    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points
    for (let points of heights) {
        points;
        f(test);
    }
    function test(t){
        console.log(t)
    }

    x = x + 10;
    y = 200 - heights[0] * 20;
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[1] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[2] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[3] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[4] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[5] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[6] * 20;
    pointString = pointString + x + "," + y + " ";
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    let newThings = []
    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;


    x = data[0];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[1];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[2];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[3];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    svgString += "</svg>"
    let output3 = document.querySelector("#output3")
    output3.innerHTML += svgString;

}


function f(i) {
   
    console.log(i);
}

